import 'package:flutter/material.dart';
import 'package:vinyearcodes/pages/main_page.dart';

void main() => runApp(const VinYearApp());

class VinYearApp extends StatelessWidget {
  static const _title = "Vin Year Codes";

  const VinYearApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      theme: ThemeData.from(
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.green,
        ),
        useMaterial3: true,
      ),
      home: const MainPage(title: _title),
    );
  }
}
