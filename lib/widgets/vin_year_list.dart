import 'package:flutter/material.dart';

class VinYearList extends StatelessWidget {
  const VinYearList({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: const <Widget>[
        _VinCodeAndYearWidget('A', '1980'),
        _ShortDivider(),
        _VinCodeAndYearWidget('B', '1981'),
        _ShortDivider(),
        _VinCodeAndYearWidget('C', '1982'),
        _ShortDivider(),
        _VinCodeAndYearWidget('D', '1983'),
        _ShortDivider(),
        _VinCodeAndYearWidget('E', '1984'),
        _ShortDivider(),
        _VinCodeAndYearWidget('F', '1985'),
        _ShortDivider(),
        _VinCodeAndYearWidget('G', '1986'),
        _ShortDivider(),
        _VinCodeAndYearWidget('H', '1987'),
        _ShortDivider(),
        _VinCodeAndYearWidget('J', '1988'),
        _ShortDivider(),
        _VinCodeAndYearWidget('K', '1989'),
        _ShortDivider(),
        _VinCodeAndYearWidget('L', '1990'),
        _ShortDivider(),
        _VinCodeAndYearWidget('M', '1991'),
        _ShortDivider(),
        _VinCodeAndYearWidget('N', '1992'),
        _ShortDivider(),
        _VinCodeAndYearWidget('P', '1993'),
        _ShortDivider(),
        _VinCodeAndYearWidget('R', '1994'),
        _ShortDivider(),
        _VinCodeAndYearWidget('S', '1995'),
        _ShortDivider(),
        _VinCodeAndYearWidget('T', '1996'),
        _ShortDivider(),
        _VinCodeAndYearWidget('V', '1997'),
        _ShortDivider(),
        _VinCodeAndYearWidget('W', '1998'),
        _ShortDivider(),
        _VinCodeAndYearWidget('X', '1999'),
        _ShortDivider(),
        _VinCodeAndYearWidget('Y', '2000'),
        _ShortDivider(),
        _VinCodeAndYearWidget('1', '2001'),
        _ShortDivider(),
        _VinCodeAndYearWidget('2', '2002'),
        _ShortDivider(),
        _VinCodeAndYearWidget('3', '2003'),
        _ShortDivider(),
        _VinCodeAndYearWidget('4', '2004'),
        _ShortDivider(),
        _VinCodeAndYearWidget('5', '2005'),
        _ShortDivider(),
        _VinCodeAndYearWidget('6', '2006'),
        _ShortDivider(),
        _VinCodeAndYearWidget('7', '2007'),
        _ShortDivider(),
        _VinCodeAndYearWidget('8', '2008'),
        _ShortDivider(),
        _VinCodeAndYearWidget('9', '2009'),
        _ShortDivider(),
        _VinCodeAndYearWidget('A', '2010'),
        _ShortDivider(),
        _VinCodeAndYearWidget('B', '2011'),
        _ShortDivider(),
        _VinCodeAndYearWidget('C', '2012'),
        _ShortDivider(),
        _VinCodeAndYearWidget('D', '2013'),
        _ShortDivider(),
        _VinCodeAndYearWidget('E', '2014'),
        _ShortDivider(),
        _VinCodeAndYearWidget('F', '2015'),
        _ShortDivider(),
        _VinCodeAndYearWidget('G', '2016'),
        _ShortDivider(),
        _VinCodeAndYearWidget('H', '2017'),
        _ShortDivider(),
        _VinCodeAndYearWidget('J', '2018'),
        _ShortDivider(),
        _VinCodeAndYearWidget('K', '2019'),
        _ShortDivider(),
        _VinCodeAndYearWidget('L', '2020'),
        _ShortDivider(),
        _VinCodeAndYearWidget('M', '2021'),
        _ShortDivider(),
        _VinCodeAndYearWidget('N', '2022'),
        _ShortDivider(),
        _VinCodeAndYearWidget('P', '2023'),
        _ShortDivider(),
        _VinCodeAndYearWidget('R', '2024'),
        _ShortDivider(),
        _VinCodeAndYearWidget('S', '2025'),
        _ShortDivider(),
        _VinCodeAndYearWidget('T', '2026'),
        _ShortDivider(),
        _VinCodeAndYearWidget('V', '2027'),
        _ShortDivider(),
        _VinCodeAndYearWidget('W', '2028'),
        _ShortDivider(),
        _VinCodeAndYearWidget('X', '2029'),
        _ShortDivider(),
        _VinCodeAndYearWidget('Y', '2030'),
        _ShortDivider(),
        _VinCodeAndYearWidget('1', '2031'),
        _ShortDivider(),
        _VinCodeAndYearWidget('2', '2032'),
        _ShortDivider(),
        _VinCodeAndYearWidget('3', '2033'),
        _ShortDivider(),
        _VinCodeAndYearWidget('4', '2034'),
        _ShortDivider(),
        _VinCodeAndYearWidget('5', '2035'),
        _ShortDivider(),
        _VinCodeAndYearWidget('6', '2036'),
        _ShortDivider(),
        _VinCodeAndYearWidget('7', '2037'),
        _ShortDivider(),
        _VinCodeAndYearWidget('8', '2038'),
        _ShortDivider(),
        _VinCodeAndYearWidget('9', '2039'),
      ],
    );
  }
}

class _VinCodeAndYearWidget extends StatelessWidget {
  static const letterStyle = TextStyle(fontSize: 22.0);
  static const yearStyle = TextStyle(fontSize: 21.0, color: Colors.grey);

  final String code;
  final String year;

  const _VinCodeAndYearWidget(this.code, this.year);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(32.0, 16.0, 24.0, 16.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(code, style: letterStyle),
            ),
            Text(year, style: yearStyle),
          ],
        ));
  }
}

class _ShortDivider extends StatelessWidget {
  static const _vertical = 0.0;
  static const _horizontal = 16.0;

  static const _padding =
      EdgeInsets.fromLTRB(_horizontal, _vertical, _horizontal, _vertical);

  const _ShortDivider();

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: _padding,
      child: Divider(),
    );
  }
}
