import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vinyearcodes/widgets/vin_year_list.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key, required this.title});

  final String title;

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  static const _showInfoKey = "show_info";

  bool _showInfo = true;

  @override
  void initState() {
    super.initState();

    _loadShowInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(icon: const Icon(Icons.info), onPressed: _showHelpDialog)
        ],
      ),
      body: _buildContent(),
    );
  }

  void _loadShowInfo() async {
    var prefs = await SharedPreferences.getInstance();
    setState(() {
      _showInfo = prefs.getBool(_showInfoKey) ?? true;
    });
  }

  Widget _buildContent() {
    if (_showInfo) {
      return Center(
        child: Column(
          children: <Widget>[
            Card(
              child: Column(
                children: <Widget>[
                  const VinInfo(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      TextButton(
                        onPressed: _hideInfo,
                        child: const Text(
                          'Dismiss',
                          style: TextStyle(color: Colors.green),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            const Expanded(
              child: VinYearList(),
            ),
          ],
        ),
      );
    }

    return const VinYearList();
  }

  Future<void> _showHelpDialog() async {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return const AlertDialog(
            title: Center(child: Text('How to find the VIN year code')),
            content: VinInfo(),
          );
        });
  }

  void _hideInfo() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setBool(_showInfoKey, false);
    setState(() {
      _showInfo = false;
    });
  }
}

class VinInfo extends StatelessWidget {
  static const _infoStyle = TextStyle(fontSize: 15.0);
  static const _vinOtherStyle = TextStyle(fontSize: 19.0, color: Colors.grey);
  static const _vinYearStyle = TextStyle(fontSize: 20.0);

  const VinInfo({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 4.0),
            child: Text(
                "The year code is the 10th digit in your vehicle's VIN code",
                textAlign: TextAlign.center,
                style: _infoStyle),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  '123456789',
                  textAlign: TextAlign.end,
                  style: _vinOtherStyle,
                ),
              ),
              Text(
                '0',
                textAlign: TextAlign.center,
                style: _vinYearStyle,
              ),
              Expanded(
                child: Text(
                  '1234567',
                  textAlign: TextAlign.start,
                  style: _vinOtherStyle,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
